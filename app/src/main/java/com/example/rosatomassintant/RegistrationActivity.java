package com.example.rosatomassintant;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegistrationActivity extends AppCompatActivity {

    private EditText userName;
    private EditText userSurname;
    private EditText userSecondName;
    private EditText email;
    private EditText password;
    private EditText passwordRepeat;
    private EditText employeeID;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        userName = findViewById(R.id.user_name);
        userSurname = findViewById(R.id.user_surname);
        userSecondName = findViewById(R.id.user_second_name);
        email = findViewById(R.id.user_email);
        password = findViewById(R.id.init_password);
        passwordRepeat = findViewById(R.id.repeat_password);
        employeeID = findViewById(R.id.user_id_workpass_number);

        mAuth = FirebaseAuth.getInstance();


        mAuthListener = new FirebaseAuth.AuthStateListener(){

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null){

                }
                else{

                }
            }
        };

    }

    public void onClickCreateProfile(View view) {
        registration(email.getText().toString(),password.getText().toString());
    }

    public void sendIntentToMainMenu(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    private void registration(String email, String password){
        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(RegistrationActivity.this, "Success Registration", Toast.LENGTH_SHORT).show();
                    sendIntentToMainMenu();
                }
                else{
                    Toast.makeText(RegistrationActivity.this, "Not Success Registration", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}